import { html } from './constants.js';


export function displayFilteredRows() {

    let rows = [...html.cardTableBody.children];

    function checkRowForShow(row) {
        let rowCode = getRowCode(row);
        let arrYearMonthDay = html.expirationDate.value.split('-');
        let codeDate = html.expirationDate.value == ''
            ? 'all'
            : arrYearMonthDay[2] + arrYearMonthDay[1] + arrYearMonthDay[0].substr(2, 2);


        let checkCategory = (html.category.value === getCodeData(rowCode)['category'])
            || (html.category.value === 'all');

        let checkAccumulation = (html.accumulation.value == getCodeData(rowCode)['accumulation'])
            || html.accumulation.value === 'all';

        let checkPercentage = (html.percentage.value === getCodeData(rowCode)['percentage'])
            || (html.percentage.value === 'all');

        let checkDate = (codeDate === getCodeData(rowCode)['expirationDate'])
            || codeDate === 'all';


        return checkCategory && checkAccumulation && checkPercentage && checkDate;
    }

    rows.forEach(row => {
        if (checkRowForShow(row)) {
            row.style.display = 'table-row';
        } else {
            row.style.display = 'none';
        }
    })

    function getRowCode(row) {
        return [...row.children][2].innerText;
    }

    function getCodeData(code) {

        let categories = { 1: 'cosmetics', 2: 'books', 3: 'accessories', 4: 'services' };

        let category = categories[code.substr(0, 1)];
        let percentage = code.substr(2, 2);
        let accumulation = code.substr(1, 1);
        let expirationDate = code.substr(4, 6);

        return { category, percentage, accumulation, expirationDate };
    }


}



