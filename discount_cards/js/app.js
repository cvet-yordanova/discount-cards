import { CardsDb } from "./CardsDb.js";
import {
  attachFilterButtonsEvents,
  attachSortButtonsEvents,
  attachEditDeleteEvents,
  attachSingleButtonsEvents,
  attachStringFormatFunc,
  attachSearchButtonsEvents,
  toggleClasses
} from "./inits.js";
import { CardsTable } from "./CardsTable.js";
import { UsersDb } from "./UsersDb.js";

const usersDb = new UsersDb();
const cardsDb = new CardsDb(usersDb);

window.localStorage.setItem("id", 0);

attachStringFormatFunc();
attachFilterButtonsEvents(cardsDb);
attachSearchButtonsEvents();
attachSortButtonsEvents(cardsDb);
attachEditDeleteEvents(cardsDb);
attachSingleButtonsEvents(cardsDb);
toggleClasses();
