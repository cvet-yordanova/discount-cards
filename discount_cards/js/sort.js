export const order = {

    sortByNameAsc: function (cardA, cardB) {
        return order.sortStringArray(cardA.name, cardB.name);
    },
    sortByNameDesc: function (cardA, cardB) {
        return order.sortStringArray(cardB.name, cardA.name);
    },
    sortByCityAsc: function (cardA, cardB) {
        return order.sortStringArray(cardA.city, cardB.city);
    },
    sortByCityDesc: function (cardA, cardB) {
        return order.sortStringArray(cardB.city, cardA.city);
    },
    sortByCodeAsc: function (cardA, cardB) {
        return order.sortStringArray(cardA.code, cardB.code);
    },
    sortByCodeDesc: function (cardA, cardB) {
        return order.sortStringArray(cardB.code, cardA.code);
    },
    sortStringArray: function (cardA, cardB) {
        if (cardA > cardB) {
            return 1;
        } else if (cardA < cardB) {
            return -1;
        }

        return 0;
    }

}




