import { CardsDb } from './CardsDb.js';
import { CardsTable } from './CardsTable.js';
import { html } from './constants.js';
import { order } from './sort.js';
import { displayFilteredRows } from './filter.js';
import { displaySearchedRows } from './search.js';
import { validateForm } from './validateForm.js';
import { UsersDb } from './UsersDb.js';


export function attachSearchButtonsEvents() {
    html.searchName.addEventListener('input', function (ev) {
        displaySearchedRows();
    });

    html.searchCity.addEventListener('input', function () {
        displaySearchedRows();
    });

    html.searchCode.addEventListener('input', function () {
        displaySearchedRows();
    });
}

export function attachFilterButtonsEvents(db) {
    category.addEventListener('change', function (ev) {
        displayFilteredRows();
    });

    accumulation.addEventListener('change', function () {
        displayFilteredRows();
    });

    percentage.addEventListener('change', function () {
        displayFilteredRows();
    });

    expirationDate.addEventListener('change', function () {
        displayFilteredRows();
    });
}

export function attachSortButtonsEvents(db) {
    html.nameAsc.addEventListener('click', function () {
        db.sort(order.sortByNameAsc);
        CardsTable.populateTableRows(db.getCardsArray());
        displayFilteredRows();
        displaySearchedRows();
        attachEditDeleteEvents(db);

    });

    html.nameDesc.addEventListener('click', function () {
        db.sort(order.sortByNameDesc);
        CardsTable.populateTableRows(db.getCardsArray());
        displayFilteredRows();
        displaySearchedRows();
        attachEditDeleteEvents(db);

    });

    html.cityAsc.addEventListener('click', function () {
        db.sort(order.sortByCityAsc);
        CardsTable.populateTableRows(db.getCardsArray());
        displayFilteredRows();
        displaySearchedRows();
        attachEditDeleteEvents(db);

    });

    html.cityDesc.addEventListener('click', function () {
        db.sort(order.sortByCityDesc);
        CardsTable.populateTableRows(db.getCardsArray());
        displayFilteredRows();
        displaySearchedRows();
        attachEditDeleteEvents(db);
    });

    html.codeAsc.addEventListener('click', function () {
        db.sort(order.sortByCodeAsc);
        CardsTable.populateTableRows(db.getCardsArray());
        displayFilteredRows();
        displaySearchedRows();
        attachEditDeleteEvents(db);
    });

    html.codeDesc.addEventListener('click', function () {
        db.sort(order.sortByCodeDesc);
        CardsTable.populateTableRows(db.getCardsArray());
        displayFilteredRows();
        displaySearchedRows();
        attachEditDeleteEvents(db);
    });
}

export function attachEditDeleteEvents(db) {
    let deleteButtons = document.getElementsByClassName("delete");
    let editButtons = document.getElementsByClassName('edit');
    let readButtons = document.getElementsByClassName('read');
    let expiredButtons = document.getElementsByClassName('expired');

    for (let i = 0; i < deleteButtons.length; i++) {
        deleteButtons[i].addEventListener('click', function () {
            db.deleteCardById(this.dataset.id);
            CardsTable.deleteRow(this.dataset.id);
        });
    }

    for (let i = 0; i < editButtons.length; i++) {
        editButtons[i].addEventListener('click', function () {
            CardsTable.fillFormData(db.getCardById(this.dataset.id));
            html.formTitle.innerText = 'Edit Card';
            html.addCardBtn.style.display = 'none';
            html.editCardBtn.style.display = 'block';
            CardsTable.showCardForm(this.dataset.id);
        });
    }

    for (let i = 0; i < readButtons.length; i++) {
        readButtons[i].addEventListener('click', function () {
            CardsTable.fillFormData(db.getCardById(this.dataset.id));

            html.formTitle.innerText = 'Read Card';
            html.addCardBtn.style.display = 'none';
            html.editCardBtn.style.display = 'none';
            CardsTable.showCardForm(this.dataset.id);
        });
    }


    for (let i = 0; i < expiredButtons.length; i++) {
        expiredButtons[i].addEventListener('click', function () {
            db.renewExpired(this.dataset.id);
            this.classList.remove('expired');
            CardsTable.populateTableRows(db.getCardsArray());
            CardsTable.populateUsersTable(JSON.parse(window.localStorage.getItem('usersDb')), db);

            html.showUsers.click();
            displayFilteredRows();
            displaySearchedRows();
            attachEditDeleteEvents(db);
        });
    }


}

export function attachSingleButtonsEvents(db) {
    html.editCardBtn.addEventListener('click', function (ev) {

        ev.preventDefault();

        if (validateForm()) {
            db.editCardById(this.dataset.id);
            CardsTable.updateRow(db.getCardById(this.dataset.id));
            CardsTable.populateUsersTable(db);
            CardsTable.populateUsersTable(JSON.parse(window.localStorage.getItem('usersDb')), db);
            attachEditDeleteEvents(db);
            html.form.reset();
            html.overlay.style.display = 'none';
            html.cardForm.style.display = 'none';
        } else {
            return;
        }

    });

    html.addCard.addEventListener('click', function (ev) {
        ev.preventDefault();
        html.formTitle.innerText = 'Add Card';
        html.addCardBtn.style.display = 'block';
        html.editCardBtn.style.display = 'none';
        CardsTable.showCardForm(null);
    });

    html.addCardBtn.addEventListener('click', function (ev) {
        ev.preventDefault();

        if (validateForm()) {
            let name = html.formName.value;
            let city = html.formCity.value;
            let email = html.formEmail.value;
            let category = html.formCategory.value;
            let accumulation = html.formAccumulation.checked ? '1' : '0';
            let percentage = html.formPercentage.value;

            let dateArr = html.formExpirationDate.value.split('-');

            db.createCard(name, city, email, category, accumulation, percentage, dateArr);
            html.form.reset();
            db.sort(order.sortByNameAsc);
            CardsTable.populateTableRows(db.getCardsArray());
            CardsTable.populateUsersTable(JSON.parse(window.localStorage.getItem('usersDb')), db);

            attachEditDeleteEvents(db);
            html.overlay.style.display = 'none';
            html.cardForm.style.display = 'none';


        } else {
            return;
        }
    });

}

export function attachStringFormatFunc() {
    String.prototype.format = function (args) {
        let str = this;
        return str.replace(String.prototype.format.regex, function (item) {
            let intVal = parseInt(item.substring(1, item.length - 1));
            let replace;
            if (intVal >= 0) {
                replace = args[intVal];
            } else if (intVal === -1) {
                replace = "{";
            } else if (intVal === -2) {
                replace = "}";
            } else {
                replace = "";
            }
            return replace;
        });
    };

    String.prototype.format.regex = new RegExp("{-?[0-9]+}", "g");

}

export function toggleClasses() {
    html.searchBox2Toggle.addEventListener('click', function () {
        html.searchBox2.classList.toggle('active');
    });

    html.searchBox3Toggle.addEventListener('click', function () {
        html.searchBox2.classList.remove('active');
        html.searchBox3.classList.toggle('active');
    });

    html.closeForm.addEventListener('click', function () {
        html.overlay.style.display = 'none';
        html.cardForm.style.display = 'none';
        html.form.reset();
    });

    html.showUsers.addEventListener('click', function () {
        html.tableCards.style.display = 'none';
        html.tableUsers.style.display = 'table';
        html.showUsers.style.display = 'none';
        html.showCards.style.display = 'block';

        document.querySelector('.search-box:nth-child(1)').style.display = 'none';
        document.querySelector('.search-box:nth-child(2)').style.display = 'none';
        document.querySelector('.search-box:nth-child(3)').style.display = 'none';


    });

    html.showCards.addEventListener('click', function () {
        html.tableCards.style.display = 'table';
        html.tableUsers.style.display = 'none';
        html.showUsers.style.display = 'block';
        html.showCards.style.display = 'none';

        document.querySelector('.search-box:nth-child(1)').style.display = 'flex';
        document.querySelector('.search-box:nth-child(2)').style.display = 'flex';
        document.querySelector('.search-box:nth-child(3)').style.display = 'flex';
    });
}


