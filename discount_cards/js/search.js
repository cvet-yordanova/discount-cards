import { html } from './constants.js';


export function displaySearchedRows() {

    let rows = [...html.cardTableBody.children];

    function checkRowForShow(row) {

        let checkName = getRowColText(row, 0)
            .toLowerCase()
            .startsWith(html.searchName.value
                .toLowerCase());

        let checkCity = getRowColText(row, 1)
            .toLowerCase()
            .startsWith(html.searchCity.value
                .toLowerCase());

        let checkCode = getRowColText(row, 2)
            .startsWith(html.searchCode.value);

        return checkName && checkCity && checkCode;
    }

    rows.forEach(row => {
        if (checkRowForShow(row)) {
            row.style.display = 'table-row';
        } else {
            row.style.display = 'none';
        }
    })

    function getRowColText(row, col) {
        return [...row.children][col].innerText;
    }

}



